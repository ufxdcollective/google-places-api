<?php

namespace UFXDCollective\GooglePlacesAPI;


class Client
{


    protected $apiURL = 'https://maps.googleapis.com/maps/api/place';
    public $apiKey;

    public function __construct($apiKey = NULL){
        $this->apiKey = $apiKey;
    }


    /**
     * @param $action
     * @param array $parameters
     * @return mixed
     * @throws \Throwable
     */
    public function request($action, $parameters = []){
        $response = $this->_fetch($action, $parameters);
        $status = $response->status ?? NULL;

        throw_unless($status === 'OK', $status);

        return $response->result;
    }


    protected function _fetch($action, $parameters = []){

        $url = "{$this->apiURL}/$action/json";
        $parameters = array_merge($parameters, [
            'key' => $this->apiKey,
        ]);

        $http = new \GuzzleHttp\Client();
        $response = $http->get($url, [
            'query' => $parameters
        ]);

        return json_decode($response->getBody()->getContents());

    }

}
