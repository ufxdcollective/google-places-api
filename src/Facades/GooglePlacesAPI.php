<?php

namespace UFXDCollective\GooglePlacesAPI\Facades;


use Illuminate\Support\Facades\Facade;

class GooglePlacesAPI extends Facade
{


    protected static function getFacadeAccessor()
    {
        return 'ufxdcollective.googleplacesapi';
    }

}
