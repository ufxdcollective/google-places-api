<?php

namespace UFXDCollective\GooglePlacesAPI;


class GooglePlacesAPI
{

    protected static $apiKey;

    public function apiKey($apiKey){
        static::$apiKey = $apiKey;
    }


    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Throwable
     */
    public static function __callStatic($name, $arguments)
    {
        $client = new Client(static::$apiKey);
        return $client->request($name, ...$arguments);
    }


    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Throwable
     */
    public function __call($name, $arguments)
    {
        $client = new Client(static::$apiKey);
        return $client->request($name, ...$arguments);
    }


}
