<?php

namespace UFXDCollective\GooglePlacesAPI\Providers;


use Illuminate\Support\ServiceProvider;
use UFXDCollective\GooglePlacesAPI\GooglePlacesAPI;

class GooglePlacesAPIServiceProvider extends ServiceProvider
{


    public function boot(){

        $this->loadMigrationsFrom(__DIR__.'/../../migrations');

        $this->publishes([
            __DIR__.'/../../migrations' => database_path('migrations'),
        ], 'migrations');


    }


    public function register()
    {

        // Bind Facades -----------------------
        $this->app->bind('ufxdcollective.googleplacesapi', GooglePlacesAPI::class);

    }



}
